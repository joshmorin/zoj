/*
    Zoj: A simple and portable binary format for UTF-8 key-value pairs
    Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

    This program is free software: you can redistribute it and/or 
    modify it under the terms of the GNU Lesser General Public 
    License as published by the Free Software Foundation, either 
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//! ‏<div lang="ar" dir="rtl" style="direction:rtl;text-align:right">
//! زوج - صيغة بسيطة للتعبير عن أزواج نصية (UTF-8 Key-Value Pairs).
//!
//! ‏</div><div lang="ar" dir="rtl" style="direction:rtl;text-align:right">
//! 
//! ‏مثال لاستخدام المكتبة: 
//! 
//! ‏</div>
//! 
//! ```
//! use zoj;
//! use std::collections::HashMap;
//! 
//! let mut source: HashMap<String, String> = HashMap::new();
//! 
//! source.insert(format!("الإسم"), format!("يوشع"));
//! source.insert(format!("مكان الإقامة"), format!("جزيرة العرب"));
//! 
//! //‎ ‫ يحول الفهرس إلى متتالي ‎`(&str, &str)`‎ ، ثم يحوله إلى كتلة بايتية
//! let blob = zoj::build(source.iter().map(|(x, y)| (x.as_ref(), y.as_ref())));
//!
//! //‎ ‫ يرد الكتلة بايتية إلى هيئته الأصلية
//! let dest = zoj::parse(&blob[..]).unwrap_or_else(|_| panic!("Malformed Zoj blob"));
//! 
//! assert!(dest.get("الإسم").unwrap() == "يوشع");
//! ```
//! 
//! ‏</div><div lang="ar" dir="rtl" style="direction:rtl;text-align:right">
//! 
//! ‏صيغة الكتلة البايتية هي: 
//! <pre>
//!   <كتلة>    :=   <طول الأزواج>   <الأزواج>
//!   <الأزواج>  :=   <مفتاح>   <قيمة>
//!   <مفتاح>   :=   <طول المفتاح>   <نص المفتاح>
//!   <قيمة>    :=   <طول القيمة>   <نص القيمة>
//!   
//!   # <طول الأزواج> و <طول الأزواج> و <طول الأزواج>: عدد صحيح عرضه ٨ بايتات بالترتيب الشبكي
//!   # <نص المفتاح> و <نص القيمة>: سلسلة محارف بترميز الـUTF-8
//! </pre>
//! 
//! ‏</div>



//! # Copyright #
//! 
//! Copyright © 2015  Josh Morin <JoshMorin@gmx.com>
//! 
//! This program is free software: you can redistribute it and/or modify
//! it under the terms of the GNU Lesser General Public License as published by
//! the Free Software Foundation, either version 3 of the License, or
//! (at your option) any later version.
//! 
//! This program is distributed in the hope that it will be useful,
//! but WITHOUT ANY WARRANTY; without even the implied warranty of
//! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//! GNU Lesser General Public License for more details.
//! 
//! You should have received a copy of the GNU General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate network_integer;

use std::collections::HashMap;
use std::{mem, str};

use network_integer as integer;
///‎ ‫ يحول متتالي ‎`(&str, &str)`‎ إلى كتلة بايتية
pub fn build<'a, 'b, T: IntoIterator<Item=(&'a str, &'b str)>>(associations: T) -> Vec<u8> {
    let mut out = vec![];
    //:‎ ‫حجز أول ٨ بايتات لتدوين المساحة الكلية للأزواج (دون أول ٨ بيتات طبعا)
    out.extend(&integer::integer_to_bytes::<u64>(0)[..]);
    //:‎ ‫تدوين الأزواج
    for (key, value) in associations {
        out.extend(&integer::integer_to_bytes(key.as_bytes().len() as u64)[..]);
        out.extend(key.as_bytes());
        out.extend(&integer::integer_to_bytes(value.as_bytes().len() as u64)[..]);
        out.extend(value.as_bytes());
    }
    let length = integer::integer_to_bytes::<u64>((out.len() - mem::size_of::<u64>()) as u64);
    //:‎ ‫تدوين المساحة أول الكتلة
    for i in 0..8 {out[i] = length[i]}
    out
}
///‎ ‫حول كتلة "زوج" إلى فهرس.
///
///‎ ‫ترجع الدالة `Err` في حالة وجود خلل في هيكل الصيغة.
pub fn parse(blob: &[u8]) -> Result<HashMap<String, String>, ()> {
    let mut blob = blob;
    let mut count : usize = 0;
    let mut map: HashMap<String, String> = HashMap::new();
    macro_rules! get_slice {
        ($len: expr) => ({
            if $len > blob.len() {return Err(())}
            let ret = &blob[..$len];
            blob = &blob[$len..];
            count += $len;
            ret
        })
    };
    macro_rules! get_len {
        () => (integer::bytes_to_integer::<u64>(get_slice!(8)).unwrap_or_else(|| panic!()))
    }
    macro_rules! get_string {
        ($len: expr) => ({
            match str::from_utf8(get_slice!($len)) {
                Ok(s) => s.to_string(), Err(_) => {return Err(())}
            }
        })
    }
    
    let overall = get_len!() as usize;
    let _ = count; //:‎ ‫رست فيه درجة من الغباء.
    count = 0; //:‎ ‫overall غير داخل في الحسبة
    while count < overall {
        let key_len = get_len!() as usize;
        let key = get_string!(key_len);
        let val_len = get_len!() as usize;
        let val = get_string!(val_len); 
        map.insert(key, val);
    }   
    
    Ok(map)
}

#[test]
fn it_works() {
    let mut map: HashMap<String, String> = HashMap::new();
    
    map.insert(format!("Hello"), format!("world"));
    map.insert(format!("Good"), format!("bye"));
    
    let blob = build(map.iter().map(|(x, y)| (x.as_ref(), y.as_ref())));
    let map2 = parse(&blob[..]).unwrap_or_else(|_| panic!("Oh no!"));
    
    assert!(map.get("Hello") == map2.get("Hello") && map2.get("Hello") == Some(&"world".to_string()));
    assert!(map.get("Good") == map2.get("Good") && map2.get("Good") == Some(&"bye".to_string()));
    
    assert!(map2.len() == 2);
    
    
    let map: HashMap<String, String> = HashMap::new();
    let blob = build(map.iter().map(|(x, y)| (x.as_ref(), y.as_ref())));
    let map2 = parse(&blob[..]).unwrap_or_else(|_| panic!("Oh no!"));
    assert!(map2.len() == 0);
}
