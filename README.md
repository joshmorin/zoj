Zoj: A simple and portable binary format and library for UTF-8 key-value pairs
==============================================================================


### Format ###
‮<كتلة>    :=   <طول الأزواج>   <الأزواج>

‮<الأزواج>  :=   <مفتاح>   <قيمة>

‮<مفتاح>   :=   <طول المفتاح>   <نص المفتاح>

‮<قيمة>    :=   <طول القيمة>   <نص القيمة>



‮<طول الأزواج> و <طول الأزواج> و <طول الأزواج>: عدد صحيح عرضه ٨ بايتات بالترتيب الشبكي.

‮<نص المفتاح> و <نص القيمة>: سلسلة محارف بترميز الـ‭UTF-8‎




### Example on Library Usage ###

    use zoj;
    use std::collections::HashMap;
     
    let mut source: HashMap<String, String> = HashMap::new();
     
    source.insert(format!("الإسم"), format!("يوشع"));
    source.insert(format!("مكان الإقامة"), format!("جزيرة العرب"));
     
    //‎ ‫ يحول الفهرس إلى متتالي ‎`(&str, &str)`‎ ، ثم يحوله إلى كتلة بايتية
    let blob = zoj::build(source.iter().map(|(x, y)| (x.as_ref(), y.as_ref())));

    //‎ ‫ يرد الكتلة بايتية إلى هيئته الأصلية
    let dest = zoj::parse(&blob[..]).unwrap_or_else(|_| panic!("Malformed Zoj blob"));
     
    assert!(dest.get("الإسم").unwrap() == "يوشع");


# Copyright #
Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
